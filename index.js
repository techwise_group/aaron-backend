const PORT = 3000;

const {promisify} = require("util");
const cors = require("cors");
const http = require("http");
const hash = require("object-hash");
const express = require("express");
const puppeteer = require("puppeteer");
const { parseString } = require("xml2js");
const { setIntervalAsync } = require("set-interval-async");
const { getStateCodeByStateName }  = require("us-state-codes");
const server = http.createServer();
const io = require("socket.io")(server, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

var current;
var browser;
var regions = {};

(async () => {
    browser = await puppeteer.launch({
        headless: true,
        args: ["--disable-setuid-sandbox"],
        ignoreHTTPSErrors: true
    });
    current = await scrape();
    server.listen(PORT, console.log(`listening on port ${PORT}`));
    setIntervalAsync(async function(){
        var next = await scrape();
        var added = [[], {}];
        var removed = [[], {}];

        next[0].forEach(function(item){if(!current[0].some(i => i.id == item.id)) added[0].push(item)});
        current[0].forEach(function(item){if(!next[0].some(i => i.id == item.id)) removed[0].push(item.id)});
        Object.entries(next[1]).forEach(function([key, value]){
            added[1][key] = [];
            if(!(key in current[1])) return;
            value.forEach(function(item){if(!current[1][key].some(i => i.id == item.id)) added[1][key].push(item)});
        });
        Object.entries(current[1]).forEach(function([key, value]){
            removed[1][key] = [];
            if(!(key in next[1])) return;
            value.forEach(function(item){if(!next[1][key].some(i => i.id == item.id)) removed[1][key].push(item.id)});
        });
        current = next;
        
        io.sockets.sockets.forEach(function(socket){
        	console.log([added[0].concat(added[1][socket.region]), removed[0].concat(removed[1][socket.region])]);
            socket.emit("face", added[0].concat(added[1][socket.region]), removed[0].concat(removed[1][socket.region]))
        });
    }, 60000);
})();

io.on("connection", async function(socket){
	console.log("connected");
	console.log(socket.handshake.query)
	var state = await fetch(`https://nominatim.openstreetmap.org/reverse?lat=${parseInt(socket.handshake.query.coords.split(",")[0])}&lon=${parseInt(socket.handshake.query.coords.split(",")[1])}`).then((response) => response.text()).then(promisify(parseString));
    socket.region = getStateCodeByStateName(state.reversegeocode.addressparts[0].state[0]);
    console.log(socket.region);
    if(!(socket.region in regions)) regions[socket.region] = 0;
    regions[socket.region]++;
    socket.emit("face", current[0].concat(current[1][socket.region]), []);
    socket.on("disconnect", function(){
        regions[socket.region]--;
    });
});

async function scrape(){
    var [page] = await browser.pages();
    await page.goto("https://www.fbi.gov/wanted/fugitives", {waitUntil: "domcontentloaded"});
    var wanted = await page.evaluate(async function(){
        await new Promise(function(resolve){
            var interval = setInterval(function(){
                document.querySelector("p.read-more").scrollIntoView();
                if(!document.querySelectorAll("p.read-more>button").length) resolve(clearInterval(interval));
            }, 1);
        });
        return Array.from(document.querySelectorAll("li.portal-type-person.castle-grid-block-item")).map(function(elem){
            return {
                name: elem.querySelector("p.name").innerText,
                crime: elem.querySelector("h3.title").innerText,
                image: elem.querySelector("img").getAttribute("src"),
                mw: "w"
            }
        });
    });
    await page.close();
    await browser.newPage();
    wanted.map((i) => ({...i, "hash": hash(i)}));

    var wantedRegions = {};
    for(var region of Object.keys(regions).filter((key) => regions[key])){
        wantedRegions[region] = await new Promise(function(resolve){
            fetch(`https://api.missingkids.org/missingkids/servlet/XmlServlet?act=rss&LanguageCountry=en_US&orgPrefix=NCMC&state=${region}`)
            .then(response => response.text())
            .then(str => parseString(str, function(err, results){
                if(err) return console.error(err);
                resolve(results.rss.channel[0].item.map(function(item){
                    var title = item.title[0].split(" ");
                    var descriptionComma = item.description[0].split(", ");
                    var descriptionSpace = item.description[0].split(" ");
                    var person = {
                        name: title.slice(1, title.length-1).join(" "),
                        missing: descriptionComma[2].split(": ")[1].split(". ")[0],
                        age: parseInt(descriptionComma[1].split(": ")[1]),
                        call: descriptionSpace.slice(-1)[0].split(".")[0],
                        image: item.enclosure[0].$.url,
                        mw: "m"
                    }
                    return {...person, id: hash(person)}
                }));
            }));
      });
    };

    return [wanted, wantedRegions];
}
